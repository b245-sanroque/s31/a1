// 1. What directive is used by Node.js in loading the modules it needs?
		// - require directive


// 2. What Node.js module contains a method for server creation?
		// - HTTP Module


// 3. What is the method of the http object responsible for creating a server using Node.js?
		// - createServer()


// 4. What method of the response object allows us to set status codes and content types?
		// - writeHead()


// 5. Where will console.log() output its contents when run in Node.js?
		// - Web Browser 


// 6. What property of the request object contains the address's endpoint?
		// - request.url



/*---*---*---*---*/


const http = require('http');

const port = 3000;

const server = http.createServer((request, response) => {


	if(request.url == '/homepage') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("You have successfully accessed the page!");
	} 

	else if(request.url == '/login') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("You are in the login page!");
	} 

	else{
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end("The Page you're trying to reach is Unavailable!");
	}

});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}`);


// nodemon routes.js
// npx kill-port 3000